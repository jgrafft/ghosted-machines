# Ghosted Machine Repository

Data simulations for Gaussian Process Classification of data from sensors attached to a hypothetical machine, currently a Friction Stir Mix Welder.

## Machine Profiles

### Error-free Operation

![Error-free Ghosted Machine Profile](visualizations/img/ghosted_machine-error_free_operation-profile_plots.png)

## Helper Functions

### `sample_from_vec`

Take samples from an `ndarray`. Replaces non-sampled indices with `0.` to preserve ordering.

![Error-free Ghosted Machine Profile with Samples](visualizations/img/ghosted_machine-error_free_operation-profile_plots_with_samples.png)


## Acknowledgements
- Martin Krasser
  - http://krasserm.github.io/2018/03/19/gaussian-processes/
