from numpy import arange, concatenate, repeat, zeros

# Local functions
from helpers import generate_path

# Mock data profiles
# 60hz sample rate for 90 seconds
sample_indices = arange(0, 5400, dtype=int)

# Sampling for 90s at 60hz
bit_angular_velocity_profile = [
    (10, 1, [1]),
    (20, 1000, [-1, 1]),
    (15, 7.5, [-1]),
    (5300, 1000, [-1, 1]),
    (30, 1000, [1]),
    (5, 2.5, [1]),
    (20, 1000, [-1, 1]),
]

forward_infrared_degrees_celcius_profile = [
    (20, 1000, [-1, 1]),
    (30, 5, [1]),
    (5295, 8250, [1]),
    (55, 75, [-1]),
]

rear_infrared_degrees_celcius_profile = [
    (20, 1000, [-1, 1]),
    (30, 0.0175, [1]),
    (5295, 8250, [1]),
    (55, 75, [-1]),
]

# Sampling for 90s at 6hz
bit_angular_velocity_profile_short = [
    (5, 1, [1]),
    (8, 1000, [-1, 1]),
    (12, 7.5, [-1]),
    (484, 1000, [-1, 1]),
    (18, 1000, [1]),
    (5, 2.5, [1]),
    (8, 1000, [-1, 1]),
]

forward_infrared_degrees_celcius_profile_short = [
    (7, 1000, [-1, 1]),
    (10, 5, [1]),
    (503, 8250, [1]),
    (20, 75, [-1]),
]

rear_infrared_degrees_celcius_profile_short = [
    (10, 1000, [-1, 1]),
    (17, 0.0095, [1]),
    (503, 8250, [1]),
    (10, 75, [-1]),
]

# Mock data streams
# 60hz sample rate for 90 seconds
bit_angular_velocity = generate_path(bit_angular_velocity_profile)

forward_infrared_degrees_celcius = generate_path(
    forward_infrared_degrees_celcius_profile, seed=19.0, normalize=False
)

rear_infrared_degrees_celcius = generate_path(
    rear_infrared_degrees_celcius_profile, seed=19.0, normalize=False
)

welder_arm_displacement = concatenate(
    (zeros(50, dtype=float), repeat(10 / 88.25, 5295), zeros(55, dtype=float))
)

# 6hz sample rate for 90 seconds ("short")
# Latent Functions
bit_angular_velocity_short = generate_path(bit_angular_velocity_profile_short)

forward_infrared_degrees_celcius_short = generate_path(
    forward_infrared_degrees_celcius_profile_short, seed=19.0, normalize=False
)

rear_infrared_degrees_celcius_short = generate_path(
    rear_infrared_degrees_celcius_profile_short, seed=29.0, normalize=False
)

welder_arm_displacement_short = concatenate(
    (zeros(10, dtype=float), repeat(10 / 88.25, 518), zeros(12, dtype=float))
)