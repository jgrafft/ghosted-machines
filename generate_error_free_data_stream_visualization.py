import altair as alt

from numpy import arange
from pandas import DataFrame

from machines.error_free_operation import (
    bit_angular_velocity,
    forward_infrared_degrees_celcius,
    rear_infrared_degrees_celcius,
    welder_arm_displacement,
)

plot_height = 150
plot_width = 1200

# Angular Velocity of Bit
bit_angular_velocity_chart = (
    alt.Chart(
        height=plot_height,
        width=plot_width,
        title="Angular Velocity (ω) of Bit",
        data=DataFrame({"x": arange(0, 5400, dtype=int), "ω": bit_angular_velocity}),
    )
    .mark_line()
    .encode(x=alt.X("x", title="t"), y="ω")
)

# Degrees Celcius Forward Infrared Temperature Sensor
forward_infrared_degrees_celcius_chart = (
    alt.Chart(
        height=plot_height,
        width=plot_width,
        title="Degrees Celcius (°C) of Forward Infrared Sensor",
        data=DataFrame(
            {
                "x": arange(0, 5400, dtype=int),
                "C": forward_infrared_degrees_celcius,
            }
        ),
    )
    .mark_line()
    .encode(x=alt.X("x", title="t"), y=alt.Y("C", title="°C"))
)

# Degrees Celcius Rear Infrared Temperature Sensor
rear_infrared_degrees_celcius_chart = (
    alt.Chart(
        height=plot_height,
        width=plot_width,
        title="Degrees Celcius (°C) of Rear Infrared Sensor",
        data=DataFrame(
            {
                "x": arange(0, 5400, dtype=int),
                "C": rear_infrared_degrees_celcius,
            }
        ),
    )
    .mark_line()
    .encode(x=alt.X("x", title="t"), y=alt.Y("C", title="°C"))
)

# Welder Arm Displacement
welder_arm_displacement_chart = (
    alt.Chart(
        height=plot_height,
        width=plot_width,
        title="Welder Arm Displacement (m/s)",
        data=DataFrame(
            {"x": arange(0, 5400, dtype=int), "m_s": welder_arm_displacement}
        ),
    )
    .mark_line()
    .encode(x=alt.X("x", title="t"), y=alt.Y("m_s", title="m/s"))
)


# vconcat charts then save
alt.vconcat(
    bit_angular_velocity_chart,
    forward_infrared_degrees_celcius_chart,
    rear_infrared_degrees_celcius_chart,
    welder_arm_displacement_chart,
).properties(title="Ghosted Machine Error-free Operation Profiles").configure_title(
    fontSize=14, anchor="middle"
).save(
    "ghosted_machine-error_free_operation-profile_plots.html"
)
