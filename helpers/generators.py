from numpy import abs, ndarray
from typing import List

from helpers import mm_norm, noise


def generate_path(segments: List, seed: float = 0, normalize: bool = True) -> ndarray:
    a = seed
    x = []

    for segment in segments:
        for i in range(segment[0]):
            a += noise(damper=segment[1], choices=segment[2])
            x.append(a)

    x = abs(x)

    return mm_norm(x) if normalize else x
