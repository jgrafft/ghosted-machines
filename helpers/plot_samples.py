import altair as alt

from numpy import arange
from pandas import DataFrame

from helpers import sample_from_vec

from machines.error_free_operation import (
    bit_angular_velocity,
    bit_angular_velocity_short,
    forward_infrared_degrees_celcius,
    forward_infrared_degrees_celcius_short,
    rear_infrared_degrees_celcius,
    rear_infrared_degrees_celcius_short,
    welder_arm_displacement,
    welder_arm_displacement_short,
)

# Sample from Latent Functions
bit_angular_velocity_samples = sample_from_vec(bit_angular_velocity, 0.01)
forward_infrared_degrees_celcius_samples = sample_from_vec(forward_infrared_degrees_celcius, 0.01)
rear_infrared_degrees_celcius_samples = sample_from_vec(rear_infrared_degrees_celcius, 0.01)
welder_arm_displacement_samples = sample_from_vec(welder_arm_displacement, 0.01)

# Sample from "short" Latent Functions
bit_angular_velocity_samples_short = sample_from_vec(bit_angular_velocity_short, 0.035)
forward_infrared_degrees_celcius_samples_short = sample_from_vec(forward_infrared_degrees_celcius_short, 0.035)
rear_infrared_degrees_celcius_samples_short = sample_from_vec(rear_infrared_degrees_celcius_short, 0.035)
welder_arm_displacement_samples_short = sample_from_vec(welder_arm_displacement_short, 0.035)

# Plots
plot_height = 120
plot_width = 860

# Angular Velocity of Bit
bit_angular_velocity_data = DataFrame(
    {
        "x": arange(0, 5400, dtype=int),
        "ω": bit_angular_velocity,
        "ω_sample": bit_angular_velocity_samples,
    }
)

bit_angular_velocity_data_stream = (
    alt.Chart(
        height=plot_height,
        width=plot_width,
        title="Angular Velocity (ω) of Bit",
        data=bit_angular_velocity_data,
    )
    .mark_line()
    .encode(x=alt.X("x", title="t"), y="ω")
)

bit_angular_velocity_points = (
    alt.Chart(bit_angular_velocity_data)
    .mark_point(color="red", shape="circle", size=12)
    .encode(
        x=alt.X("x", title="t"),
        y=alt.Y("ω_sample", title="ω"),
    )
    .transform_filter(alt.FieldGTPredicate(field="ω_sample", gt=0))
)

bit_angular_velocity_chart = (
    bit_angular_velocity_data_stream + bit_angular_velocity_points
)

# Degrees Celcius Forward Infrared Temperature Sensor
forward_infrared_degrees_celcius_data = DataFrame(
    {
        "x": arange(0, 5400, dtype=int),
        "C": forward_infrared_degrees_celcius,
        "C_sample": forward_infrared_degrees_celcius_samples,
    }
)

forward_infrared_degrees_celcius_data_stream = (
    alt.Chart(
        height=plot_height,
        width=plot_width,
        title="Degrees Celcius (°C) of Forward Infrared Sensor",
        data=forward_infrared_degrees_celcius_data,
    )
    .mark_line()
    .encode(x=alt.X("x", title="t"), y=alt.Y("C", title="°C"))
)

forward_infrared_degrees_celcius_points = (
    alt.Chart(forward_infrared_degrees_celcius_data)
    .mark_point(color="red", shape="circle", size=12)
    .encode(
        x=alt.X("x", title="t"),
        y="C_sample",
    )
    .transform_filter(alt.FieldGTPredicate(field="C_sample", gt=0))
)


forward_infrared_degrees_celcius_chart = (
    forward_infrared_degrees_celcius_data_stream
    + forward_infrared_degrees_celcius_points
)

# Degrees Celcius Rear Infrared Temperature Sensor
rear_infrared_degrees_celcius_data = DataFrame(
    {
        "x": arange(0, 5400, dtype=int),
        "C": rear_infrared_degrees_celcius,
        "C_sample": rear_infrared_degrees_celcius_samples,
    }
)

rear_infrared_degrees_celcius_data_stream = (
    alt.Chart(
        height=plot_height,
        width=plot_width,
        title="Degrees Celcius (°C) of Rear Infrared Sensor",
        data=rear_infrared_degrees_celcius_data,
    )
    .mark_line()
    .encode(x=alt.X("x", title="t"), y=alt.Y("C", title="°C"))
)

rear_infrared_degrees_celcius_points = (
    alt.Chart(rear_infrared_degrees_celcius_data)
    .mark_point(color="red", shape="circle", size=12)
    .encode(
        x=alt.X("x", title="t"),
        y="C_sample",
    )
    .transform_filter(alt.FieldGTPredicate(field="C_sample", gt=0))
)

rear_infrared_degrees_celcius_chart = (
    rear_infrared_degrees_celcius_data_stream + rear_infrared_degrees_celcius_points
)

# Welder Arm Displacement
welder_arm_displacement_data = DataFrame(
    {
        "x": arange(0, 5400, dtype=int),
        "m_s": welder_arm_displacement,
        "m_s_sample": welder_arm_displacement_samples,
    }
)

welder_arm_displacement_data_stream = (
    alt.Chart(
        height=plot_height,
        width=plot_width,
        title="Welder Arm Displacement (m/s)",
        data=welder_arm_displacement_data,
    )
    .mark_line()
    .encode(x=alt.X("x", title="t"), y=alt.Y("m_s", title="m/s"))
)

welder_arm_displacement_points = (
    alt.Chart(welder_arm_displacement_data)
    .mark_point(color="red", shape="circle", size=12)
    .encode(
        x=alt.X("x", title="t"),
        y="m_s_sample",
    )
    .transform_filter(alt.FieldGTPredicate(field="m_s_sample", gt=0))
)

welder_arm_displacement_chart = (
    welder_arm_displacement_data_stream + welder_arm_displacement_points
)

# "Short" Samples
# Angular Velocity of Bit
bit_angular_velocity_data_short = DataFrame(
    {
        "x": arange(0, 540, dtype=int),
        "ω": bit_angular_velocity_short,
        "ω_sample": bit_angular_velocity_samples_short,
    }
)

bit_angular_velocity_data_stream_short = (
    alt.Chart(
        height=plot_height,
        width=plot_width,
        title="Angular Velocity (ω) of Bit",
        data=bit_angular_velocity_data_short,
    )
    .mark_line()
    .encode(x=alt.X("x", title="t"), y="ω")
)

bit_angular_velocity_points_short = (
    alt.Chart(bit_angular_velocity_data_short)
    .mark_point(color="red", shape="circle", size=12)
    .encode(
        x=alt.X("x", title="t"),
        y=alt.Y("ω_sample", title="ω"),
    )
    .transform_filter(alt.FieldGTPredicate(field="ω_sample", gt=0))
)

bit_angular_velocity_chart_short = (
    bit_angular_velocity_data_stream_short + bit_angular_velocity_points_short
)

# Degrees Celcius Forward Infrared Temperature Sensor
forward_infrared_degrees_celcius_data_short = DataFrame(
    {
        "x": arange(0, 540, dtype=int),
        "C": forward_infrared_degrees_celcius_short,
        "C_sample": forward_infrared_degrees_celcius_samples_short,
    }
)

forward_infrared_degrees_celcius_data_stream_short = (
    alt.Chart(
        height=plot_height,
        width=plot_width,
        title="Degrees Celcius (°C) of Forward Infrared Sensor",
        data=forward_infrared_degrees_celcius_data_short,
    )
    .mark_line()
    .encode(x=alt.X("x", title="t"), y=alt.Y("C", title="°C"))
)

forward_infrared_degrees_celcius_points_short = (
    alt.Chart(forward_infrared_degrees_celcius_data_short)
    .mark_point(color="red", shape="circle", size=12)
    .encode(
        x=alt.X("x", title="t"),
        y="C_sample",
    )
    .transform_filter(alt.FieldGTPredicate(field="C_sample", gt=0))
)

forward_infrared_degrees_celcius_chart_short = (
    forward_infrared_degrees_celcius_data_stream_short
    + forward_infrared_degrees_celcius_points_short
)

# Degrees Celcius Rear Infrared Temperature Sensor
rear_infrared_degrees_celcius_data_short = DataFrame(
    {
        "x": arange(0, 540, dtype=int),
        "C": rear_infrared_degrees_celcius_short,
        "C_sample": rear_infrared_degrees_celcius_samples_short,
    }
)

rear_infrared_degrees_celcius_data_stream_short = (
    alt.Chart(
        height=plot_height,
        width=plot_width,
        title="Degrees Celcius (°C) of Rear Infrared Sensor",
        data=rear_infrared_degrees_celcius_data_short,
    )
    .mark_line()
    .encode(x=alt.X("x", title="t"), y=alt.Y("C", title="°C"))
)

rear_infrared_degrees_celcius_points_short = (
    alt.Chart(rear_infrared_degrees_celcius_data_short)
    .mark_point(color="red", shape="circle", size=12)
    .encode(
        x=alt.X("x", title="t"),
        y="C_sample",
    )
    .transform_filter(alt.FieldGTPredicate(field="C_sample", gt=0))
)

rear_infrared_degrees_celcius_chart_short = (
    rear_infrared_degrees_celcius_data_stream_short + rear_infrared_degrees_celcius_points_short
)

# Welder Arm Displacement
welder_arm_displacement_data_short = DataFrame(
    {
        "x": arange(0, 540, dtype=int),
        "m_s": welder_arm_displacement_short,
        "m_s_sample": welder_arm_displacement_samples_short,
    }
)

welder_arm_displacement_data_stream_short = (
    alt.Chart(
        height=plot_height,
        width=plot_width,
        title="Welder Arm Displacement (m/s)",
        data=welder_arm_displacement_data_short,
    )
    .mark_line()
    .encode(x=alt.X("x", title="t"), y=alt.Y("m_s", title="m/s"))
)

welder_arm_displacement_points_short = (
    alt.Chart(welder_arm_displacement_data_short)
    .mark_point(color="red", shape="circle", size=12)
    .encode(
        x=alt.X("x", title="t"),
        y="m_s_sample",
    )
    .transform_filter(alt.FieldGTPredicate(field="m_s_sample", gt=0))
)

welder_arm_displacement_chart_short = (
    welder_arm_displacement_data_stream_short + welder_arm_displacement_points_short
)

# vconcat charts
error_free_machine_profile_plot = alt.vconcat(
    bit_angular_velocity_chart,
    forward_infrared_degrees_celcius_chart,
    rear_infrared_degrees_celcius_chart,
    welder_arm_displacement_chart,
).properties(
    title="Ghosted Machine Error-free Operation Profiles with Samples"
).configure_title(
    fontSize=14, anchor="middle"
)

error_free_machine_profile_plot_short = alt.vconcat(
    bit_angular_velocity_chart_short,
    forward_infrared_degrees_celcius_chart_short,
    rear_infrared_degrees_celcius_chart_short,
    welder_arm_displacement_chart_short,
).properties(
    title="Ghosted Machine Error-free Operation Profiles with Samples"
).configure_title(
    fontSize=14, anchor="middle"
)