from numpy import ndarray, vectorize
from numpy.random import choice, random
from typing import List


def mm_norm(X: ndarray) -> ndarray:
    return (X - X.min()) / (X.max() - X.min())


def noise(damper: int = 10, choices: List[int] = [-1, 1]) -> float:
    return choice(choices) * (random() / damper)


def sample_from_vec(V: ndarray, threshold: float = 0.03) -> ndarray:
    return vectorize(lambda v: v if random() < threshold else 0.0)(V)
